#include <string>
#include <windows.h>
#include <atlbase.h>
#include <hlink.h>
#include <intshcut.h>
#include <shobjidl.h>
#include "resource.h"

void Log(const wchar_t *format, ...) {
  wchar_t linebuf[1024];
  va_list v;
  va_start(v, format);
  wvsprintf(linebuf, format, v);
  va_end(v);
  OutputDebugString(linebuf);
}

class IELaunch {
  HWND window_;
  std::wstring target_;

public:
  IELaunch(HWND window) : window_(window) {}

  void UpdateTarget(HWND control) {
    int n = GetWindowTextLength(control);
    if (WCHAR *buf = new WCHAR[n + 1]) {
      GetWindowText(control, buf, n + 1);
      target_ = buf;
      delete [] buf;
    }
  }

  void HLink() {
    HRESULT hr;
    CComPtr<IHlink> link;
    CComPtr<IBindCtx> bind;

    IHlink *linkRaw;
    hr = HlinkCreateFromString(target_.c_str(),
                               nullptr,
                               nullptr,
                               nullptr,
                               0,
                               nullptr,
                               IID_IHlink,
                               reinterpret_cast<void**>(&linkRaw));
    if (SUCCEEDED(hr)) {
      link.Attach(linkRaw);
      linkRaw = nullptr;
    }
    else {
      Log(L"HlinkCreateFromString failed - %08x\n", hr);
      return;
    }

    IBindCtx *bindRaw;
    hr = CreateBindCtx(0, &bindRaw);
    if (SUCCEEDED(hr)) {
      bind.Attach(bindRaw);
      bindRaw = nullptr;
    }
    else {
      Log(L"CreateBindCtx failed - %08x\n", hr);
      return;
    }

    DWORD flags = HLNF_OPENINNEWWINDOW;
    hr = HlinkNavigate(link, nullptr, flags, bind, nullptr, nullptr);
    if (FAILED(hr)) {
      Log(L"HlinkNavigate failed - %08x\n", hr);
      return;
    }
  }

  void InvokeCommand() {
    HRESULT hr;
    IUniformResourceLocator *urlRaw;
    CComPtr<IUniformResourceLocator> url;
    hr = CoCreateInstance(CLSID_InternetShortcut,
                          nullptr,
                          CLSCTX_INPROC_SERVER,
                          IID_IUniformResourceLocator,
                          (void**)&urlRaw);
    if (SUCCEEDED(hr)) {
      url.Attach(urlRaw);
      urlRaw = nullptr;
    }
    else {
      Log(L"CoCreateInstance(CLSID_InternetShortcut) failed - %08x\n", hr);
      return;
    }

    if (CComQIPtr<IPersistFile> file = url) {
      hr = file->Load(target_.c_str(), 0);
      if (FAILED(hr)) {
        Log(L"IPersistFile::Load failed - %08x\n", hr);
        return;
      }
    }

    if (CComQIPtr<IContextMenu> menu = url) {
      CMINVOKECOMMANDINFO info{};
      info.cbSize = sizeof(info);
      info.hwnd = window_;
      info.nShow = SW_SHOWNORMAL;
      hr = menu->InvokeCommand(&info);
      if (FAILED(hr)) {
        Log(L"IContextMenu::InvokeCommand failed - %08x\n", hr);
        return;
      }
    }
  }

  void Shell() {
    SHELLEXECUTEINFOW info{};
    info.cbSize = sizeof(info);
    info.hwnd = window_;
    info.nShow = SW_SHOWNORMAL;
    info.lpFile = target_.c_str();
    ShellExecuteEx(&info);
  }
};

INT_PTR CALLBACK DlgProc(HWND dlg, UINT msg, WPARAM w, LPARAM) {
  IELaunch launcher(dlg);
  switch (msg) {
  case WM_INITDIALOG:
    SetDlgItemText(dlg, IDC_EDIT1, L"https://kernel.org");
    return TRUE;
  case WM_COMMAND:
    switch (LOWORD(w)) {
    case IDOK:
    case IDCANCEL:
      EndDialog(dlg, LOWORD(w));
      break;
    case IDC_SHELLEXE:
      launcher.UpdateTarget(GetDlgItem(dlg, IDC_EDIT1));
      launcher.Shell();
      break;
    case IDC_HLINK:
      launcher.UpdateTarget(GetDlgItem(dlg, IDC_EDIT1));
      launcher.HLink();
      break;
    case IDC_INVOKE:
      launcher.UpdateTarget(GetDlgItem(dlg, IDC_EDIT1));
      launcher.InvokeCommand();
      break;
    }
    break;
  }
  return 0;
}

int APIENTRY wWinMain(HINSTANCE inst, HINSTANCE, LPWSTR, int) {
  HRESULT hr;
  hr = CoInitialize(nullptr);
  if (SUCCEEDED(hr)) {
    DialogBox(inst, MAKEINTRESOURCE(IDD_DIALOG1), nullptr, DlgProc);
    CoUninitialize();
  }
  return 0;
}
